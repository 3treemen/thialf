<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/
//For example, you can paste this into your theme functions.php file

function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}

add_action( 'wp_footer', 'meks_which_template_is_loaded' );




function add_customjs() {
	wp_enqueue_script( 'customjs', get_stylesheet_directory_uri().'/assets/js/child.js', array('jquery'), 2, true );
    wp_enqueue_style( 'print', get_stylesheet_directory_uri() . '/assets/css/print.css',false,'1.1','all');
 }
 add_action( 'wp_enqueue_scripts', 'add_customjs', 99999 );


/**
 * Register extra widget area in footer
 *
 */
function thialf_widgets_init() {

	register_sidebar( array(
		'name'          => 'Footer Social',
		'id'            => 'footer-social',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'thialf_widgets_init' );



function get_opening_hours() {

    $api_key = 'AIzaSyC_Rjs0TOzq0Nt2hRpL4fguQkOwGP8uFpU';
    
    $brasserie_place_id = 'ChIJz7zeu0ikx0cRbus8Uxkej6k';
    $bso_place_id = 'ChIJaaDXyWSlx0cRixMVkuOVk5E';
    $playground_place_id = 'ChIJ4VTCokikx0cRaSYDGGcw5g8';

    $places = array(
        $brasserie_place_id,
        $playground_place_id,        
    );
    echo '<div id=openingstijden">';
    echo '<div class="openings-title">Openingstijden vandaag</div>';
    foreach($places as $place) {
        $url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' . $place . '&fields=opening_hours%2Cname%2Cformatted_phone_number&language=nl&key='. $api_key . '';
        $json = file_get_contents($url);
        $jo = json_decode($json, true);
        // echo $url;
        $status = $jo['status'];
        $result = $jo['result'];
		echo '<div class="location-wrapper">';
		echo '<span class="location-name">' . $result['name'] . '</span>';

		// get the day opening hours from the Google Json
        $days = $result['opening_hours']['weekday_text'];
        $open_now = $result['opening_hours']['open_now'];
		// the php-day week starts on Sunday (0), but Google maps returns Monday as 0
        // so we need to decrement the php day to match the Google day 
        $current_day = date('w');
        if($current_day == 0 ) {
            $current_day == 6;
        }
        else {
            $current_day--;
        }
    
        $today = $days[$current_day];
        $today_open = substr($today, strpos($today,':') +1);
            echo '<span class="location-openinghours">' . $today_open . '</span>';


        echo '</div>';
    }
    echo '</div>';

}


function wp_add_openinghours_shortcode() {
	ob_start();
	get_opening_hours();
	return ob_get_clean();
}
add_shortcode( 'openinghours', 'wp_add_openinghours_shortcode' );


//* Redirect homepage on mobile
add_action( 'wp_head', 'wps_params', 10 );
function wps_params() {
    ?>
    
	<script>
	if (window.location.pathname == '/' && jQuery(window).width() <= 480) {
	   window.location = "/agenda/";
	}
	</script>

    <?php
}



/**
 * registration_limit_restriction
 * @param $method,$post
 * @return $method
 * @since 3.1.11
 */
function your_child_theme_registration_disable($method,$post){
    $disable_button = get_post_meta($post->ID,'_show_hide_registration_button',true);
     
    //disable button if settings in event meta box
    if( $disable_button == 0){
         return false;
    }else{
        return $method;
    }
}
add_filter('display_event_registration_method','your_child_theme_registration_disable',90,2);
add_filter('get_event_registration_method','your_child_theme_registration_disable',90,2);
  
  
  
  
add_filter( 'submit_event_form_fields', 'show_hide_registration_button' );
function show_hide_registration_button( $fields) {  
      
    $fields['event']['show_hide_registration_button'] =  array(
                                        'label'       => __( 'Show Hide Registration Button', 'wp-event-manager-registrations' ),
                                        'type'        => 'checkbox',
                                        'required'    => false,
                                        'priority'    => 22,
                                   );
    return $fields;
}

/**
 * Redirect user after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
            // redirect them to the default place
            return $redirect_to;
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}
 
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


add_filter( 'avf_modify_thumb_size', 'enfold_customization_modify_thumb_size', 10, 1 );

function enfold_customization_modify_thumb_size( $size ) {
    $size['entry_with_sidebar'] = array('width'=>800, 'height'=>600);
    // $size['portfolio'] = array('width'=>495, 'height'=>400);
    return $size;
}