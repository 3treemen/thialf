<?php
global $post;
$start_date = get_event_start_date();
$start_time = get_event_start_time();
$end_date   = get_event_end_date();
$end_time   = get_event_end_time();
$event_type = get_event_type();
if (is_array($event_type) && isset($event_type[0]))
    $event_type = $event_type[0]->slug;

$thumbnail  = get_event_thumbnail($post,'full');
$organizer_id = get_post_meta(get_the_id(), '_event_organizer_ids');

foreach ($organizer_id as $v) {
    foreach($v as $a){
        $organizer = $a;
    }
}
if ($organizer == 681) {
        $organizer_name = 'Sportbedrijf Arnhem';
        $org_icon = '/wp-content/themes/thialf/assets/icon-playground.svg';
        $org_icon_blue = '/wp-content/themes/thialf/assets/icon-playground-blue.svg';
    
} elseif ($organizer == 675) {
        $organizer_name = 'BSO';
        $org_icon = '/wp-content/themes/thialf/assets/icon-bso.svg';
        $org_icon_blue = '/wp-content/themes/thialf/assets/icon-bso-blue.svg';

} elseif($organizer == 688) {
        $organizer_name = 'Brasserie';
        $org_icon = '/wp-content/themes/thialf/assets/icon-brasserie.svg';
        $org_icon_blue = '/wp-content/themes/thialf/assets/icon-brasserie-blue.svg';

} elseif($organizer == 678 || !$organizer) {
    $organizer_name = 'Vrijstaat Thialf';
    $org_icon = '/wp-content/themes/thialf/assets/icon-stichting.png';
    $org_icon_blue = '/wp-content/themes/thialf/assets/icon-stichting.png';

}




// echo 'organiser'. $organizer_id;




?>

<div class="wpem-event-box-col wpem-col wpem-col-12 wpem-col-md-6 wpem-col-lg-<?php echo apply_filters('event_manager_event_wpem_column', '4'); ?>">
    <!----- wpem-col-lg-4 value can be change by admin settings ------->
    <div class="wpem-event-layout-wrapper">
        <div <?php event_listing_class(''); ?>>
        <a href="<?php display_event_permalink(); ?>" class="wpem-event-action-url event-style-color org-icon <?php echo $event_type; ?>" >
                <div class="icon">
                    <div>
                        <span class="iconwrapper grey"><img src="<?php echo $org_icon; ?>" alt="" /></span>
                        <span class="iconwrapper blue" style="display: none;"><img src="<?php echo $org_icon_blue; ?>" alt="" /></span>
                    </div>
                </div>

                <div class="wpem-event-infomation">
                    <div class="wpem-event-date">
                        <div class="wpem-event-date-type">
                            <?php
                            if (!empty($start_date))
                            {
                                ?>
                                <div class="wpem-from-date">
                                    <div class="wpem-date"><?php echo date_i18n('d', strtotime($start_date)); ?></div>
                                    <div class="wpem-month"><?php echo date_i18n('M', strtotime($start_date)); ?></div>
                                </div>
                            <?php } ?>

                            <?php
                            if ($start_date != $end_date && !empty($end_date))
                            {
                                ?>
                                <div class="wpem-to-date">
                                    <div class="wpem-date-separator">-</div>
                                    <div class="wpem-date"><?php echo date_i18n('d', strtotime($end_date)); ?></div>
                                    <div class="wpem-month"><?php echo date_i18n('M', strtotime($end_date)); ?></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="wpem-event-details">
                        <?php do_action('wpem_event_listing_event_detail_start',$post->ID);?>
                        <div class="wpem-event-title"><h5 class="wpem-heading-text"><?php echo esc_html(get_the_title()); ?></h3></div>

                        <div class="wpem-event-date-time">
                            <span class="wpem-event-date-time-text">
                                <?php display_event_start_date(); ?> 
                                <?php
                                if (!empty($start_time))
                                {
                                    echo '<br>';// display_date_time_separator();
                                }
                                ?>
                                <?php display_event_start_time(); ?>
                                <?php
                                if (!empty($end_date) || !empty($end_time))
                                {
                                    ?> - <?php } ?> 

                                <?php
                                if (isset($start_date) && isset($end_date) && $start_date != $end_date)
                                {
                                    display_event_end_date();
                                }
                                ?> 
                                <?php
                                if (!empty($end_date) && !empty($end_time))
                                {
                                    display_date_time_separator();
                                }
                                ?> 
                                <?php display_event_end_time(); ?>
                            </span>
                        </div>
                        <div class="wpem-event-location">
                            <span class="wpem-event-location-text">
                                <?php
                                if (get_event_location() == 'Online Event' || get_event_location() == ''): echo __('Online Event', 'wp-event-manager');
                                else: display_event_location(false);
                                endif;
                                ?>
                            </span>
                        </div>
                 
                        <div class="wpem-event-shortdescription">
                            <?php
                                $shortdescription = get_post_meta( get_the_ID(), '_listing_description', true);
                                echo $shortdescription;
                            ?>
                        </div>
                    </div>
                </div>   

                <div class="wpem-event-banner">
                    <div class="wpem-event-banner-img" style="background-image: url('<?php echo $thumbnail ?>')">

                        <!-- Hide in list View // Show in Box View -->
                        <?php do_action('event_already_registered_title'); ?>     
                        <div class="wpem-event-date">
                            <div class="wpem-event-date-type">
                                <?php
                                if (!empty($start_date))
                                {
                                    ?>
                                    <div class="wpem-from-date">
                                        <div class="wpem-date"><?php echo date_i18n('d', strtotime($start_date)); ?></div>
                                        <div class="wpem-month"><?php echo date_i18n('M', strtotime($start_date)); ?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- Hide in list View // Show in Box View -->
                    </div>
                </div>
            </a>   
                
        </div>
    </div>
</div>
<script>
jQuery(document).ready(function($){
    $(".wpem-event-action-url").hover(function(){
        $(this).find(".wpem-event-shortdescription").addClass('show');
        // console.log($(this).children(".wpem-event-shortdescription"));
        // $(this).find(".wpem-event-shortdescription").toggle();
        // $(this).find(".iconwrapper").toggle();
    }, function() {
        $(this).find(".wpem-event-shortdescription").removeClass('show');
    });
});
</script>
