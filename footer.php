		<div class="container_wrapper" id="naw">
			<div class="container">
			<svg id="logo_1_wit" data-name="logo 1 wit" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="185.588" height="19.768" viewBox="0 0 185.588 19.768">
  <defs>
    <clipPath id="clip-path">
      <rect id="Rectangle_2" data-name="Rectangle 2" width="185.588" height="19.768" transform="translate(0 0)" fill="#fff"/>
    </clipPath>
  </defs>
  <g id="Group_2" data-name="Group 2" transform="translate(0 0)" clip-path="url(#clip-path)">
    <path id="Path_645" data-name="Path 645" d="M43.5,19.264a.386.386,0,0,1-.127.2.39.39,0,0,1-.252.072H39.753a.4.4,0,0,1-.252-.072.506.506,0,0,1-.144-.252L36.082,6.25a.475.475,0,0,1,.08-.288.351.351,0,0,1,.3-.108H39a.485.485,0,0,1,.26.072.434.434,0,0,1,.172.27l1.854,7.848a.241.241,0,0,0,.08.135.168.168,0,0,0,.081.027q.126,0,.162-.162L43.461,6.2a.442.442,0,0,1,.171-.27.492.492,0,0,1,.262-.072h2.537a.353.353,0,0,1,.3.108.468.468,0,0,1,.08.288Z" fill="#fff"/>
    <path id="Path_646" data-name="Path 646" d="M51.328,5.854a8.262,8.262,0,0,1,2.106.279,5.891,5.891,0,0,1,1.926.891,4.893,4.893,0,0,1,1.413,1.557,4.5,4.5,0,0,1,.549,2.3,6.053,6.053,0,0,1-.756,3.006,5.668,5.668,0,0,1-.783,1.089,8.6,8.6,0,0,1-.909.855q-.126.108-.126.162-.018.09.162.18l2.322,1.476a.557.557,0,0,1,.27.486V19.1a.621.621,0,0,1-.072.3.315.315,0,0,1-.306.135H54.946a.751.751,0,0,1-.3-.045,1.386,1.386,0,0,1-.225-.135l-2.286-1.71a.436.436,0,0,0-.234-.108q-.09,0-.108.126l-.414,1.566a.364.364,0,0,1-.189.243.616.616,0,0,1-.261.063H49.006a.635.635,0,0,1-.252-.054q-.126-.054-.126-.342V6.376a.675.675,0,0,1,.09-.423.456.456,0,0,1,.342-.1Zm.576,8.37a.226.226,0,0,0,.108.2l.2.144a.32.32,0,0,0,.144.036.147.147,0,0,0,.108-.036,4.948,4.948,0,0,0,1.1-1.494,4.809,4.809,0,0,0,.378-2.034,3.338,3.338,0,0,0-.2-1.233,2.324,2.324,0,0,0-.5-.792,1.819,1.819,0,0,0-.621-.423,1.617,1.617,0,0,0-.576-.126q-.144,0-.144.162Z" fill="#fff"/>
    <path id="Path_647" data-name="Path 647" d="M62.991,19.1a.545.545,0,0,1-.081.315.357.357,0,0,1-.315.117H60.039a.427.427,0,0,1-.324-.1.459.459,0,0,1-.09-.315V6.3a.487.487,0,0,1,.1-.36.459.459,0,0,1,.315-.09h2.5q.45,0,.45.468Z" fill="#fff"/>
    <path id="Path_648" data-name="Path 648" d="M72.261,5.854a.648.648,0,0,1,.324.072q.126.072.126.342v7.848a7.112,7.112,0,0,1-.387,2.457,5.057,5.057,0,0,1-1.062,1.764A4.41,4.41,0,0,1,69.7,19.408a5.021,5.021,0,0,1-1.9.36,6.711,6.711,0,0,1-1.377-.189,5.285,5.285,0,0,1-1.7-.729q-.27-.18-.306-.306a.2.2,0,0,1,.018-.144c.024-.047.048-.1.072-.144l.846-1.584a.257.257,0,0,1,.216-.144.425.425,0,0,1,.18.054,4.406,4.406,0,0,0,.783.369,2.119,2.119,0,0,0,.675.153,2.349,2.349,0,0,0,.837-.153,1.725,1.725,0,0,0,.72-.531,2.728,2.728,0,0,0,.495-1.017,5.97,5.97,0,0,0,.18-1.593V8.878q0-.324-.288-.324H65.439q-.378,0-.378-.216a1.491,1.491,0,0,1,.036-.162l.612-2.07a.346.346,0,0,1,.378-.252Z" fill="#fff"/>
    <path id="Path_649" data-name="Path 649" d="M74.583,19.534q-.36,0-.36-.288a.571.571,0,0,1,.036-.162l.666-2.106a.487.487,0,0,1,.522-.306h2.394a1,1,0,0,0,.576-.171.655.655,0,0,0,.252-.585,1.467,1.467,0,0,0-.086-.441,2.382,2.382,0,0,0-.256-.531l-2.154-3.186a9.693,9.693,0,0,1-.805-1.413,3.242,3.242,0,0,1-.317-1.359,2.87,2.87,0,0,1,.972-2.3A3.807,3.807,0,0,1,78.6,5.854h3.528q.324,0,.324.27a.546.546,0,0,1-.018.162L81.8,8.266a.413.413,0,0,1-.448.288H78.972a1.246,1.246,0,0,0-.484.072c-.107.048-.161.162-.161.342a.888.888,0,0,0,.17.477c.113.174.23.351.349.531l2.419,3.69a4.859,4.859,0,0,1,.708,1.5,5.083,5.083,0,0,1,.152,1.107,3.361,3.361,0,0,1-.242,1.278,2.924,2.924,0,0,1-.708,1.035,3.3,3.3,0,0,1-1.155.693,4.6,4.6,0,0,1-1.585.252Z" fill="#fff"/>
    <path id="Path_650" data-name="Path 650" d="M89.325,8.554q-.162,0-.162.18V19.1a.6.6,0,0,1-.072.315q-.072.117-.324.117H86.283q-.252,0-.324-.1a.625.625,0,0,1-.072-.351V8.752q0-.2-.162-.2H83.133q-.342,0-.342-.27a.571.571,0,0,1,.036-.162l.63-1.98a.415.415,0,0,1,.45-.288h8.064q.324,0,.324.27a.546.546,0,0,1-.018.162l-.63,1.98a.415.415,0,0,1-.45.288Z" fill="#fff"/>
    <path id="Path_651" data-name="Path 651" d="M102.248,19.138a.312.312,0,0,1,.018.126.276.276,0,0,1-.072.189.3.3,0,0,1-.234.081H99.62a.421.421,0,0,1-.279-.072.844.844,0,0,1-.171-.252l-.486-.972a.162.162,0,0,0-.144-.108H95.246a.128.128,0,0,0-.126.09l-.468.972q-.18.342-.432.342H91.844a.3.3,0,0,1-.243-.081.3.3,0,0,1-.063-.189.312.312,0,0,1,.018-.126l3.42-12.96a.419.419,0,0,1,.135-.243.4.4,0,0,1,.261-.081h3.042a.388.388,0,0,1,.261.072.411.411,0,0,1,.117.2Zm-6.462-3.492a.34.34,0,0,0-.018.108q0,.126.108.126H97.91q.108,0,.108-.126A.34.34,0,0,0,98,15.646l-.936-4.212q-.036-.162-.162-.162-.144,0-.162.162Z" fill="#fff"/>
    <path id="Path_652" data-name="Path 652" d="M113.265,19.138a.312.312,0,0,1,.018.126.276.276,0,0,1-.072.189.3.3,0,0,1-.234.081h-2.34a.421.421,0,0,1-.279-.072.844.844,0,0,1-.171-.252l-.486-.972a.162.162,0,0,0-.144-.108h-3.294a.128.128,0,0,0-.126.09l-.468.972q-.18.342-.432.342h-2.376a.3.3,0,0,1-.243-.081.3.3,0,0,1-.063-.189.312.312,0,0,1,.018-.126l3.42-12.96a.419.419,0,0,1,.135-.243.4.4,0,0,1,.261-.081h3.042a.388.388,0,0,1,.261.072.411.411,0,0,1,.117.2ZM106.8,15.646a.34.34,0,0,0-.018.108q0,.126.108.126h2.034q.108,0,.108-.126a.34.34,0,0,0-.018-.108l-.936-4.212q-.036-.162-.162-.162-.144,0-.162.162Z" fill="#fff"/>
    <path id="Path_653" data-name="Path 653" d="M119.133,8.554q-.162,0-.162.18V19.1a.6.6,0,0,1-.072.315q-.072.117-.324.117h-2.484q-.252,0-.324-.1a.625.625,0,0,1-.072-.351V8.752q0-.2-.162-.2h-2.592q-.342,0-.342-.27a.571.571,0,0,1,.036-.162l.63-1.98a.415.415,0,0,1,.45-.288h8.064q.324,0,.324.27a.546.546,0,0,1-.018.162l-.63,1.98a.415.415,0,0,1-.45.288Z" fill="#fff"/>
    <path id="Path_654" data-name="Path 654" d="M133.875,8.554q-.162,0-.162.18V19.1a.6.6,0,0,1-.072.315q-.072.117-.324.117h-2.484q-.252,0-.324-.1a.625.625,0,0,1-.072-.351V8.752q0-.2-.162-.2h-2.592q-.342,0-.342-.27a.571.571,0,0,1,.036-.162l.63-1.98a.415.415,0,0,1,.45-.288h8.064q.324,0,.324.27a.546.546,0,0,1-.018.162l-.63,1.98a.415.415,0,0,1-.45.288Z" fill="#fff"/>
    <path id="Path_655" data-name="Path 655" d="M144.638,11.9q0-.2-.162-.2H142.1q-.162,0-.162.18l-.2,7.218a.545.545,0,0,1-.081.315.337.337,0,0,1-.3.117H139.04q-.306,0-.36-.126a.83.83,0,0,1-.054-.324V6.3a.487.487,0,0,1,.1-.36.459.459,0,0,1,.315-.09h2.322a.408.408,0,0,1,.45.45l.108,2.808q0,.216.18.216h2.376q.18,0,.18-.2l.09-2.808a.424.424,0,0,1,.468-.468h2.3a.459.459,0,0,1,.315.09.487.487,0,0,1,.1.36v12.78a.83.83,0,0,1-.054.324q-.054.126-.36.126h-2.3a.36.36,0,0,1-.315-.117.545.545,0,0,1-.081-.315Z" fill="#fff"/>
    <path id="Path_656" data-name="Path 656" d="M154.646,19.1a.545.545,0,0,1-.081.315.357.357,0,0,1-.315.117h-2.556a.427.427,0,0,1-.324-.1.459.459,0,0,1-.09-.315V6.3a.487.487,0,0,1,.1-.36.459.459,0,0,1,.315-.09h2.5q.45,0,.45.468Z" fill="#fff"/>
    <path id="Path_657" data-name="Path 657" d="M167.174,19.138a.316.316,0,0,1,.017.126.276.276,0,0,1-.072.189.3.3,0,0,1-.233.081h-2.34a.417.417,0,0,1-.279-.072.826.826,0,0,1-.171-.252l-.487-.972a.162.162,0,0,0-.143-.108h-3.294a.126.126,0,0,0-.126.09l-.469.972c-.12.228-.263.342-.431.342H156.77a.254.254,0,0,1-.306-.27.312.312,0,0,1,.018-.126L159.9,6.178a.43.43,0,0,1,.135-.243.4.4,0,0,1,.262-.081h3.042a.389.389,0,0,1,.261.072.427.427,0,0,1,.117.2Zm-6.462-3.492a.315.315,0,0,0-.019.108c0,.084.037.126.109.126h2.034c.071,0,.107-.042.107-.126a.338.338,0,0,0-.017-.108l-.937-4.212c-.023-.108-.078-.162-.162-.162s-.149.054-.161.162Z" fill="#fff"/>
    <path id="Path_658" data-name="Path 658" d="M169.424,19.534q-.306,0-.36-.126a.83.83,0,0,1-.054-.324V6.3a.482.482,0,0,1,.1-.36.455.455,0,0,1,.314-.09h2.448q.45,0,.45.468V16.4q0,.2.063.234a.3.3,0,0,0,.153.036h3.4q.342,0,.342.288a.546.546,0,0,1-.018.162l-.666,2.106a.458.458,0,0,1-.486.306Z" fill="#fff"/>
    <path id="Path_659" data-name="Path 659" d="M180.747,19.1c-.024.288-.134.432-.324.432h-2.34q-.307,0-.36-.126a.811.811,0,0,1-.054-.324V6.3a.483.483,0,0,1,.1-.36.456.456,0,0,1,.315-.09h7.181a.307.307,0,0,1,.235.1.348.348,0,0,1,.09.243V8.248a.262.262,0,0,1-.11.225.379.379,0,0,1-.232.081h-3.925c-.155,0-.233.066-.233.2v.072l.142,1.6c0,.1.061.144.181.144h2.664a.156.156,0,0,1,.136.063.271.271,0,0,1,.045.153v.072l-.163,1.692a.216.216,0,0,1-.235.2h-2.466c-.12,0-.179.054-.179.162Z" fill="#fff"/>
  </g>
  <g id="Group_5" data-name="Group 5" transform="translate(0 0)">
    <g id="Group_4" data-name="Group 4" clip-path="url(#clip-path)">
      <path id="Path_660" data-name="Path 660" d="M30.988,11.715a.956.956,0,0,0-.545.169L25.87,5.834,24.2,5.886l-.067-1.105a1.842,1.842,0,1,0-1.056.023L23.01,5.886l-1.418.01L16.858,1.451A.953.953,0,0,0,16.987.97a.969.969,0,1,0-.97.97.958.958,0,0,0,.515-.149l4.8,6.249.264,2.653-3.711,7.73-1.305.365-.314.732,3.389,0-.151-1.154,4.1-6.192,4.1,6.192-.151,1.154,3.39,0-.314-.732-1.306-.365-3.711-7.73.555-2.04,3.961,3.592a.959.959,0,0,0-.106.44.97.97,0,1,0,.969-.97" fill="#fff"/>
      <path id="Path_661" data-name="Path 661" d="M.97,11.715a.959.959,0,0,1,.546.169l4.572-6.05,1.672.052.067-1.105A1.841,1.841,0,1,1,8.882,4.8l.066,1.082,1.419.01L15.1,1.451A.953.953,0,0,1,14.971.97a.972.972,0,1,1,.455.821l-4.8,6.249-.264,2.653,3.711,7.73,1.306.365.313.732-3.389,0,.15-1.154-4.1-6.192-4.1,6.192.151,1.154-3.39,0,.314-.732,1.306-.365,3.711-7.73-.555-2.04L1.833,12.245a.96.96,0,0,1,.107.44.97.97,0,1,1-.97-.97" fill="#fff"/>
    </g>
  </g>
</svg>
		<address>Dullertstraat 33, 6828 HJ Arnhem</address>
		<?php
			$social_args = array( 'outside'=>'ul', 'inside'=>'li', 'append' => '' );
			echo avia_social_media_icons( $social_args, false );

		?>
			</div>
	</div>
		<?php
		
		if ( ! defined( 'ABSPATH' ) ) {  exit;  }    // Exit if accessed directly
			
		
		do_action( 'ava_before_footer' );	
			
		global $avia_config;
		$blank = isset( $avia_config['template'] ) ? $avia_config['template'] : '';

		//reset wordpress query in case we modified it
		wp_reset_query();


		//get footer display settings
		$the_id = avia_get_the_id(); //use avia get the id instead of default get id. prevents notice on 404 pages
		$footer = get_post_meta( $the_id, 'footer', true );
		$footer_options = avia_get_option( 'display_widgets_socket', 'all' );
		
		//get link to previous and next post/portfolio entry
		$avia_post_nav = avia_post_nav();

		/**
		 * Reset individual page override to defaults if widget or page settings are different (user might have changed theme options)
		 * (if user wants a page as footer he must select this in main options - on individual page it's only possible to hide the page)
		 */
		if( false !== strpos( $footer_options, 'page' ) )
		{
			/**
			 * User selected a page as footer in main options
			 */
			if( ! in_array( $footer, array( 'page_in_footer_socket', 'page_in_footer', 'nofooterarea' ) ) ) 
			{
				$footer = '';
			}
		}
		else
		{
			/**
			 * User selected a widget based footer in main options
			 */
			if( in_array( $footer, array( 'page_in_footer_socket', 'page_in_footer' ) ) ) 
			{
				$footer = '';
			}
		}
		
		$footer_widget_setting 	= ! empty( $footer ) ? $footer : $footer_options;

		/*
		 * Check if we should display a page content as footer
		 */
		if( ! $blank && in_array( $footer_widget_setting, array( 'page_in_footer_socket', 'page_in_footer' ) ) )
		{
			/**
			 * Allows 3rd parties to change page id's, e.g. translation plugins
			 */
			$post = AviaCustomPages()->get_custom_page_object( 'footer_page', '' );
			
			if( ( $post instanceof WP_Post ) && ( $post->ID != $the_id ) )
			{
				/**
				 * Make sure that footerpage is set to fullwidth
				 */
				$old_avia_config = $avia_config;
				
				$avia_config['layout']['current'] = array(
											'content'	=> 'av-content-full alpha', 
											'sidebar'	=> 'hidden', 
											'meta'		=> '', 
											'entry'		=> '',
											'main'		=> 'fullsize'
										);    
				
				$builder_stat = ( 'active' == Avia_Builder()->get_alb_builder_status( $post->ID ) );
				$avia_config['conditionals']['is_builder'] = $builder_stat;
				$avia_config['conditionals']['is_builder_template'] = $builder_stat;
				
				/**
				 * @used_by			config-bbpress\config.php
				 * @since 4.5.6.1
				 * @param WP_Post $post
				 * @param int $the_id
				 */
				do_action( 'ava_before_page_in_footer_compile', $post, $the_id );
				
				$content = Avia_Builder()->compile_post_content( $post );
				
				$avia_config = $old_avia_config;
				
				/**
				 * @since 4.7.4.1
				 * @param string 
				 * @param WP_Post $post
				 * @param int $the_id
				 */
				$extra_class = apply_filters( 'avf_page_as_footer_extra_classes', 'container_wrap footer-page-content footer_color', $post, $the_id );
				
				/**
				 * Wrap footer page in case we need extra CSS changes 
				 * 
				 * @since 4.7.4.1
				 */
				echo '<div class="' . $extra_class . '" id="footer-page">';
				echo	$content;
				echo '</div>';
			}
		}
		
		/**
		 * Check if we should display a footer
		 */
		if( ! $blank && $footer_widget_setting != 'nofooterarea' )
		{
			if( in_array( $footer_widget_setting, array( 'all', 'nosocket' ) ) )
			{
				//get columns
				$columns = avia_get_option('footer_columns');
		?>
				<div class='container_wrap footer_color' id='footer'>

					<div class='container'>

						<?php
						do_action('avia_before_footer_columns');

						//create the footer columns by iterating
				        switch( $columns )
				        {
				        	case 1: 
								$class = ''; 
								break;
				        	case 2: 
								$class = 'av_one_half'; 
								break;
				        	case 3: 
								$class = 'av_one_third'; 
								break;
				        	case 4: 
								$class = 'av_one_fourth'; 
								break;
				        	case 5: 
								$class = 'av_one_fifth'; 
								break;
				        	case 6: 
								$class = 'av_one_sixth'; 
								break;
							default: 
								$class = ''; 
								break;
				        }
				        
				        $firstCol = "first el_before_{$class}";

						//display the footer widget that was defined at appearenace->widgets in the wordpress backend
						//if no widget is defined display a dummy widget, located at the bottom of includes/register-widget-area.php
						for( $i = 1; $i <= $columns; $i++ )
						{
							$class2 = ''; // initialized to avoid php notices
							if( $i != 1 ) 
							{
								$class2 = " el_after_{$class}  el_before_{$class}";
							}
							
							echo "<div class='flex_column {$class} {$class2} {$firstCol}'>";
							
							if( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'Footer - column' . $i ) ) : else : avia_dummy_widget( $i ); endif;
							
							echo '</div>';
							
							$firstCol = '';
						}

						do_action( 'avia_after_footer_columns' );

	?>
				<?php if ( is_active_sidebar( 'footer-social' ) ) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-social' ); ?>
					</div><!-- #primary-sidebar -->
				<?php endif; ?>
					</div>

				<!-- ####### END FOOTER CONTAINER ####### -->
				</div>

	<?php   } //endif   array( 'all', 'nosocket' ) ?>


	<?php

			//copyright
			$copyright = do_shortcode( avia_get_option( 'copyright', '&copy; ' . __( 'Copyright', 'avia_framework' ) . "  - <a href='" . home_url( '/' ) . "'>" . get_bloginfo('name') . '</a>' ) );

			// you can filter and remove the backlink with an add_filter function
			// from your themes (or child themes) functions.php file if you dont want to edit this file
			// you can also remove the kriesi.at backlink by adding [nolink] to your custom copyright field in the admin area
			// you can also just keep that link. I really do appreciate it ;)
			$kriesi_at_backlink = kriesi_backlink( get_option( THEMENAMECLEAN . "_initial_version" ), 'Enfold' );


			if( $copyright && strpos( $copyright, '[nolink]' ) !== false )
			{
				$kriesi_at_backlink = '';
				$copyright = str_replace( '[nolink]', '', $copyright );
			}
			
			/**
			 * @since 4.5.7.2
			 * @param string $copyright
			 * @param string $copyright_option
			 * @return string
			 */
			$copyright_option = avia_get_option( 'copyright' );
			$copyright = apply_filters( 'avf_copyright_info', $copyright, $copyright_option );

			if( in_array( $footer_widget_setting, array( 'all', 'nofooterwidgets', 'page_in_footer_socket' ) ) )
			{

			?>
				
				<footer class='container_wrap socket_color' id='socket' <?php avia_markup_helper( array( 'context' => 'footer' ) ); ?>>
                    <div class='container'>

                        <span class='copyright'><?php echo $copyright . $kriesi_at_backlink; ?></span>

                        <?php
                        	if( avia_get_option( 'footer_social', 'disabled' ) != 'disabled' )
                            {
                            	$social_args = array( 'outside'=>'ul', 'inside'=>'li', 'append' => '' );
								echo avia_social_media_icons( $social_args, false );
                            }

							$avia_theme_location = 'avia3';
							$avia_menu_class = $avia_theme_location . '-menu';

							$args = array(
										'theme_location'	=> $avia_theme_location,
										'menu_id'			=> $avia_menu_class,
										'container_class'	=> $avia_menu_class,
										'fallback_cb'		=> '',
										'depth'				=> 1,
										'echo'				=> false,
										'walker'			=> new avia_responsive_mega_menu( array( 'megamenu' => 'disabled' ) )
									);

                            $menu = wp_nav_menu( $args );
                            
                            if( $menu )
							{ 
								echo "<nav class='sub_menu_socket' " . avia_markup_helper( array( 'context' => 'nav', 'echo' => false ) ) . '>';
								echo	$menu;
								echo '</nav>';
							}
                        ?>

                    </div>

	            <!-- ####### END SOCKET CONTAINER ####### -->
				</footer>
				


			<?php
			} //end nosocket check - array( 'all', 'nofooterwidgets', 'page_in_footer_socket' )


		
		
		} //end blank & nofooterarea check
		?>
		<!-- end main -->
		</div>
		
		<?php
		
		
		//display link to previous and next portfolio entry
		echo	$avia_post_nav;
		
		echo "<!-- end wrap_all --></div>";


		if( isset( $avia_config['fullscreen_image'] ) )
		{ ?>
			<!--[if lte IE 8]>
			<style type="text/css">
			.bg_container {
			-ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale')";
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php echo $avia_config['fullscreen_image']; ?>', sizingMethod='scale');
			}
			</style>
			<![endif]-->
		<?php
			echo "<div class='bg_container' style='background-image:url(" . $avia_config['fullscreen_image'] . ");'></div>";
		}
	?>




<a href='#top' title='<?php _e( 'Scroll to top', 'avia_framework' ); ?>' id='scroll-top-link' <?php echo av_icon_string( 'scrolltop' ); ?>><span class="avia_hidden_link_text"><?php _e( 'Scroll to top', 'avia_framework' ); ?></span></a>

<div id="fb-root"></div>

<?php

	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
	
?>

</body>
</html>
